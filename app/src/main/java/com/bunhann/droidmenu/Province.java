package com.bunhann.droidmenu;

public class Province {

    private int id;
    private String name;
    private double s;

    public Province(int id, String name, double s) {
        this.id = id;
        this.name = name;
        this.s = s;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getS() {
        return s;
    }

    public void setS(double s) {
        this.s = s;
    }

    @Override
    public String toString() {
        return "1";
    }
}
